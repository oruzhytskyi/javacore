package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class RightParenthesis extends AbstractToken {
    public RightParenthesis() {
        super(Tokens.RIGHT_PARENTHESIS);
    }
}
