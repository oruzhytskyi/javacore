package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Division extends BinaryExpression {
    public Division(Expression firstArg, Expression secondArg) {
        super(firstArg, secondArg);
    }

    public int evaluate(int value) {
        try {
            return firstArg.evaluate(value) / secondArg.evaluate(value);
        } catch (DBZException | OverflowException e) {
            throw e;
        } catch (ArithmeticException e) {
                throw new DBZException();
        }
    }
}
