package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Variable implements Expression {
    private final String name;

    public Variable(String name) {
        this.name = name;
    }

    public int evaluate(int value) {
        return value;
    }
}
