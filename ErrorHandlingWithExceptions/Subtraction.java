package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Subtraction extends BinaryExpression {
    public Subtraction(Expression firstArg, Expression secondArg) {
        super(firstArg, secondArg);
    }

    public int evaluate(int value) {
        try {
            return Math.subtractExact(
                firstArg.evaluate(value), secondArg.evaluate(value));
        } catch (ArithmeticException e) {
            throw new OverflowException();
        }
    }
}
