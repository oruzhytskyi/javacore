package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Minus extends Operator {
    public Minus() {
        super(Tokens.MINUS);
    }
}
