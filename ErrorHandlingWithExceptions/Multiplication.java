package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Multiplication extends BinaryExpression {
    public Multiplication(Expression firstArg, Expression secondArg) {
        super(firstArg, secondArg);
    }

    public int evaluate(int value) {
        try {
            return Math.multiplyExact(
                firstArg.evaluate(value), secondArg.evaluate(value));
        } catch (DBZException | OverflowException e) {
            throw e;
        } catch (ArithmeticException e) {
            throw new OverflowException();
        }
    }
}
