package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class SyntaxErrorException extends Exception {
    public SyntaxErrorException(String message) {
        super(message);
    }
};
