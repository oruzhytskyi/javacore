package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public abstract class Operator extends AbstractToken {
    public Operator(String strRepr) {
        super(strRepr);
    }

    public int precedence() {
        return Tokens.PREC_CATEGORIES.size() - Tokens.PRECEDENCE.get(strRepr);
    }
}
