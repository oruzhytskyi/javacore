package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Number extends AbstractToken {
    private final int value;

    public Number(String strRepr) {
        super(strRepr);
        value = Integer.parseInt(strRepr);
    }

    public Number(int value) {
        super(String.valueOf(value));
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
