package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public abstract class BinaryExpression implements Expression {
    protected Expression firstArg;
    protected Expression secondArg;

    public BinaryExpression(Expression firstArg, Expression secondArg) {
        this.firstArg = firstArg;
        this.secondArg = secondArg;
    }
}
