package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Plus extends Operator {
    public Plus() {
        super(Tokens.PLUS);
    }
}
