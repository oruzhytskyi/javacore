package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public interface Expression {
    int evaluate(int value);
}
