package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public abstract class AbstractToken implements Token {
    protected String strRepr;

    public AbstractToken(String strRepr) {
        this.strRepr = strRepr;
    }

    public boolean isOperator() {
        return Tokens.OPERATORS.contains(strRepr);
    }

    public boolean isParenthesis() {
        return Tokens.PARENTHESES.contains(strRepr);
    }

    public boolean isVariable() {
        return Tokens.VARIABLES.contains(strRepr);
    }

    public boolean isNumber() {
        return this instanceof Number ? true: false;
    }

    public String toString() {
        return strRepr;
    }
}
