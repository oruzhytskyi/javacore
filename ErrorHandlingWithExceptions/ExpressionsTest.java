package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class ExpressionsTest {
    public static void main (String[] args) {
        Expression expr = new Subtraction(
            new Multiplication(
                new Const(2),
                new Variable("x")
            ),
            new Const(3)
        );
        System.out.println(expr.evaluate(5));
        System.out.println(0.0/0.0);
    }
}
