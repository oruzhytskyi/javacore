package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Addition extends BinaryExpression {
    public Addition(Expression firstArg, Expression secondArg) {
        super(firstArg, secondArg);
    }

    public int evaluate(int value) {
        try {
            return Math.addExact(
                firstArg.evaluate(value), secondArg.evaluate(value));
        } catch (ArithmeticException e) {
            throw new OverflowException();
        }
    }
}
