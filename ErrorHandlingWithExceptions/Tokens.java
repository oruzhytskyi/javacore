package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public final class Tokens {
    public static final String PLUS = "+";
    public static final String MINUS = "-";
    public static final String MULT = "*";
    public static final String DIV = "/";
    public static final String LEFT_PARENTHESIS = "(";
    public static final String RIGHT_PARENTHESIS = ")";
    public static final String X_VAR = "x";

    // This list contains all valid lexems defined above
    public static final ArrayList<String> LEXEMS = new ArrayList<>(
        Arrays.asList(PLUS, MINUS, MULT, DIV, LEFT_PARENTHESIS,
                      RIGHT_PARENTHESIS, X_VAR ));

    public static final int MAX_LEXEME_SIZE;

    static {
        int maxSize = 0;
        for (String lexeme: LEXEMS) {
            if (lexeme.length() > maxSize) maxSize = lexeme.length();
        }
        MAX_LEXEME_SIZE = maxSize;
    }

    // Operators are placed into array according to their precedence
    // in descending order. Thus, operators with higher priority appear
    // lesser indexes in array.
    public static final ArrayList<ArrayList<String>> PREC_CATEGORIES = new ArrayList<>(
        Arrays.asList(
            new ArrayList<>(Arrays.asList(MULT, DIV)),
            new ArrayList<>(Arrays.asList(PLUS, MINUS))
        )
    );

    // Just add new group of operators to list above and PRECEDENCE will
    // be correctly initialized automatically
    public static final HashMap<String, Integer> PRECEDENCE;

    static {
        PRECEDENCE = new HashMap<>();
        for (int i = 0; i < PREC_CATEGORIES.size(); i++) {
            for (String token: PREC_CATEGORIES.get(i)) {
                PRECEDENCE.put(token, i);
            }
        }
    }

    public static final ArrayList<String> OPERATORS = new ArrayList<>(
        Arrays.asList(MULT, DIV, PLUS, MINUS));

    public static final ArrayList<String> PARENTHESES = new ArrayList<>(
        Arrays.asList(LEFT_PARENTHESIS, RIGHT_PARENTHESIS));

    public static final ArrayList<String> VARIABLES = new ArrayList<>(
        Arrays.asList(X_VAR));

    private Tokens() {};

    public static Token makeToken(String strRepr) {
        if (strRepr.equals(Tokens.PLUS)) {
            return new Plus();
        } else if (strRepr.equals(Tokens.MINUS)) {
            return new Minus();
        } else if (strRepr.equals(Tokens.MULT)) {
            return new Mult();
        } else if (strRepr.equals(Tokens.DIV)) {
            return new Div();
        } else if (strRepr.equals(Tokens.LEFT_PARENTHESIS)) {
            return new LeftParenthesis();
        } else if (strRepr.equals(Tokens.RIGHT_PARENTHESIS)) {
            return new RightParenthesis();
        } else if (strRepr.equals(Tokens.X_VAR)) {
            return new Var(Tokens.X_VAR);
        } else {
            return new Number(strRepr);
        }
    }

    public static boolean isLexeme(String str) {
        return Tokens.LEXEMS.contains(str);
    }
}
