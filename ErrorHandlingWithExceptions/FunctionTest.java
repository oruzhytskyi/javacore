package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class FunctionTest {
    public static void main(String[] args) {
        checkFuncEquals("3*x+1", 5, 16);
        checkFuncEquals("2000*x", 10, 20000);
        checkFuncEquals("-(2-x)+1", 5, 4);
        checkFuncEquals("2-(x+1)/(1+x)", 10, 1);
        checkFuncEquals("(x+1)/(-1+x)", 2, 3);
        checkFuncEquals("-2-(x+1)", 10, -13);
        checkFuncEquals("-x+1", 10, -9);
        checkThrowsSyntaxError("");
    }

    public static int calculate(String functionExpr, int xvar) 
            throws SyntaxErrorException {
        int result = (new Function(functionExpr)).evaluate(xvar);
        System.out.println("For x=" + xvar + ": " + functionExpr + "=" + result);
        return result;
    }

    public static void checkFuncEquals(String funcExpr, int xvar, int expVal) {
        try {
            int result = calculate(funcExpr, xvar);
            if (result - expVal < 0.0001) {
                System.out.println("PASSED");
            } else {
                System.out.println("FAILED");
                System.out.printf("Expected: %d, got %d\n", expVal, result);
            }
        } catch (Exception e) {
            System.out.println("FAILED");
            System.out.println("Got exception:");
            System.out.println(e);
        }
    }

    public static void checkThrowsSyntaxError(String funcExpr) {
        try {
            calculate(funcExpr, 0);
            System.out.println("FAILED");
            System.out.println("Expected SyntaxErrorException, but didn't get one");
        } catch (SyntaxErrorException e) {
            System.out.println("PASSED");
        } catch (Exception e) {
            System.out.println("FAILED");
            System.out.println("Expected SyntaxErrorException, but got: " + e);
        }
    }

}
