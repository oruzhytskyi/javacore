package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public interface Token {
    boolean isOperator();
    boolean isParenthesis();
    boolean isVariable();
    boolean isNumber();
}
