package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class Const implements Expression {
    private final int value;

    public Const(int value) {
        this.value = value;
    }

    public int evaluate(int value) {
        return this.value;
    }
}
