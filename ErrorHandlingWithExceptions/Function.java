package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Stack;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;


public class Function {
    private final Expression expression;
    private final String strRepr;

    public Function(File inputDescr)
            throws SyntaxErrorException, FileNotFoundException,
                   IOException {
        try (BufferedReader input = new BufferedReader(
                                new FileReader(inputDescr))) {
            String line = input.readLine();
            strRepr = line;
            if (line != null) {
                expression = parseFunc(line);
            } else {
                throw new SyntaxErrorException("No function provided");
            }
        }
    }

    public Function(String strRepr)
            throws SyntaxErrorException {
        this.strRepr = strRepr;
        expression = parseFunc(strRepr);
    }

    private Expression parseFunc(String strRepr)
            throws SyntaxErrorException {
        ArrayList<Token> tokens = tokenize(strRepr);
        System.out.println(tokens);
        ArrayList<Token> tokensInRPN = toRPN(tokens);
        System.out.println(tokensInRPN);
        return makeExpression(tokensInRPN);
    }

    public void write(File outputDescr) throws IOException {
        try (PrintWriter writer = new PrintWriter(
                                new FileWriter(outputDescr))) {
            int value;
            for (int x = 0; x < 11; x++) {
                try {
                    value = evaluate(x);
                    writer.println(x + "        " + value);
                } catch (DBZException e) {
                    writer.println(x + "        division by zero");
                } catch (OverflowException e) {
                    writer.println(x + "        overflow");
                }
            }
        }
    }

    public String toString() {
        return strRepr;
    }

    public int evaluate(int xvar) {
        return expression.evaluate(xvar);
    }

    private ArrayList<Token> tokenize(String strRepr)
            throws SyntaxErrorException {
        ArrayList<Token> rawTokens = new ArrayList<>();
        String curSequence= "";
        String curNumber = "";
        for (Character symbol: strRepr.toCharArray()) {
            if (Character.isDigit(symbol)) {
                curNumber += symbol;
            } else {
                if (!curNumber.equals("")) {
                    rawTokens.add(Tokens.makeToken(curNumber));
                    curNumber = "";
                }
                curSequence += symbol;
                if (Tokens.isLexeme(curSequence)) {
                    rawTokens.add(Tokens.makeToken(curSequence));
                    curSequence = "";
                } else if (curSequence.length() > Tokens.MAX_LEXEME_SIZE) {
                    throw new SyntaxErrorException(curSequence);
                }
            }
        }

        if (!curNumber.equals("")) {
            rawTokens.add(Tokens.makeToken(curNumber));
        }

        // Replace minuses with "+(-1)*" and then remove pluses which
        // are not placed between numbers or variables. This approach
        // may be not optimal in performace and memory regard, but
        // is more clear for understanding and less error prone than
        // handling different situations with minuses.
        ArrayList<Token> tokens = new ArrayList<>();
        Token prevToken = null;
        for (Token token: rawTokens) {
            if (token instanceof Minus) {
                if (prevToken != null
                    && (prevToken instanceof Number
                        || prevToken instanceof Var)) {
                    tokens.add(new Plus());
                }
                tokens.add(new Number(-1));
                tokens.add(new Mult());
            }
            else {
                tokens.add(token);
            }
            prevToken = token;
        }

        return tokens;
    }

    private ArrayList<Token> toRPN(ArrayList<Token> tokens)
            throws SyntaxErrorException {
        ArrayList<Token> outputQueue = new ArrayList<>();
        Stack<Token> operatorStack = new Stack<>();
        
        for (Token token: tokens) {
            if (token.isNumber() || token.isVariable()) {
                outputQueue.add(token);
            } else if (token.isOperator()) {
                while (!operatorStack.empty() &&
                       operatorStack.peek().isOperator() &&
                       ((Operator) token).precedence() <=
                       ((Operator) operatorStack.peek()).precedence()) {
                    outputQueue.add(operatorStack.pop());
                }
                operatorStack.push(token);
            } else if (token instanceof LeftParenthesis) {
                operatorStack.push(token); 
            } else if (token instanceof RightParenthesis) {
                try {
                    while (!(operatorStack.peek() instanceof LeftParenthesis)) {
                        outputQueue.add(operatorStack.pop());
                    }
                } catch (EmptyStackException e) {
                    throw new SyntaxErrorException("Mismatch parentheses");
                }
                try {
                    operatorStack.pop();
                } catch (EmptyStackException e) {
                    throw new SyntaxErrorException("Syntax error");
                }
                
            } else {
                throw new SyntaxErrorException(token.toString());
            }
        }

        while (!operatorStack.empty()) {
            if (operatorStack.peek().isParenthesis()) {
                throw new SyntaxErrorException("Mismatch parentheses");
            } else {
                outputQueue.add(operatorStack.pop());
            }
        }

        return outputQueue;
    }

    private Expression makeExpression(ArrayList<Token> tokenQueue) {
        Stack<Expression> exprStack = new Stack<>();
        for (Token token: tokenQueue) {
            System.out.println("Token: " + token);
            System.out.println("Expr stack: " + exprStack);
            if (token.isNumber()) {
                exprStack.push(new Const(((Number) token).getValue()));
            } else if (token.isVariable()) {
                exprStack.push(new Variable(token.toString())); 
            } else if (token.isOperator()) {
                Expression secondArg = exprStack.pop();
                Expression firstArg = exprStack.pop();
                if (token instanceof Plus) {
                    exprStack.push(new Addition(firstArg, secondArg));
                } else if (token instanceof Minus) {
                    exprStack.push(new Subtraction(firstArg, secondArg));
                } else if (token instanceof Mult) {
                    exprStack.push(new Multiplication(firstArg, secondArg));
                } else if (token instanceof Div) {
                    exprStack.push(new Division(firstArg, secondArg));
                } else {
                }
            } else {
            }
        }
        return exprStack.pop();
    }
}
