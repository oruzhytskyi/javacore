package JavaAcademy.JavaCore.ErrorHandlingWithExceptions;

public class LeftParenthesis extends AbstractToken {
    public LeftParenthesis() {
        super(Tokens.LEFT_PARENTHESIS);
    }
}
