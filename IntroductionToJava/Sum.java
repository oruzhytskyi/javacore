public class Sum {
    public static void main(String[] args) {
        int sum = 0;
        for (String arg: args) {
            for (String part: arg.split("\\s+")) {
                sum += Integer.parseInt(part); 
            }
        }
        System.out.println(sum);
    }
}
