public class LinkedQueue<E> extends AbstractQueue<E> {
    private Node<E> head;
    private Node<E> tail;

    private class Node<E> {
        private E value;
        private Node<E> prev;

        public Node(E value, Node<E> prev) {
            this.value = value;
            this.prev = prev;
        }
    }

    public LinkedQueue(int capacity) {
        super(capacity);
    }

    private boolean ensureCapacity() {
        return !(size == capacity);
    }

    public boolean add(E element) {
        // TODO: Handle OutOfMemoryError exception.
        if (!ensureCapacity()) {
            return false;
        } else {
            Node<E> newNode = new Node<E>(element, tail);
            tail = newNode;
            if (head == null) head = tail;
            return true;
        }
    }

    public E remove() {
        if (tail == null) {
            return null;
        } else {
            Node<E> oldTail = tail;
            tail = tail.prev;
            return oldTail.value;
        }
    }

}
