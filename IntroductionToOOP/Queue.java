public interface Queue<E> {
    boolean add(E element);
    E remove();
    int getCapacity();
}
