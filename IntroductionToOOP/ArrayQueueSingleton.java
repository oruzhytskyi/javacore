public final class ArrayQueueSingleton {
    private ArrayQueueSingleton() {};
    public static final ArrayQueue queue = new ArrayQueue(10);
}
