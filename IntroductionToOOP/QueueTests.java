public class QueueTests {
    public static void testQueueIsFilledCorrectly(Queue queue) {
        int queueLength = queue.getCapacity();
        for (int i = 0; i < queueLength; i++) {
            queue.add(i);
        } 
        for (int i = 0; i < queueLength; i++) {
            assert (int) queue.remove() == i;
        } 
        System.out.println("PASSED");
    }

    public static void testQueueOverflow(Queue queue) {
        int queueLength = queue.getCapacity();
        for (int i = 0; i < queueLength; i++) {
            queue.add(i);
        } 
        assert !queue.add(1);
        System.out.println("PASSED");
    }

    public static void testRemoveFromEmptyQueue(Queue queue) {
        int queueLength = queue.getCapacity();
        assert queue.remove() == null;
        System.out.println("PASSED");
    }

    public static void testQueueUnderflow(Queue queue) {
        int queueLength = queue.getCapacity();
        for (int i = 0; i < queueLength; i++) {
            queue.add(i);
        } 
        for (int i = 0; i < queueLength; i++) {
            assert (int) queue.remove() == i;
        } 
        assert queue.remove() == null;
        System.out.println("PASSED");
    }

    public static void testQueueSingleton() {
        int queueLength = 7;
        for (int i = 0; i < queueLength; i++) {
            ArrayQueueSingleton.queue.add(i);
        } 
        for (int i = 0; i < queueLength; i++) {
            assert (int) ArrayQueueSingleton.queue.remove() == i;
        } 
        assert ArrayQueueSingleton.queue.remove() == null;
        System.out.println("PASSED");
    }

    public static void main (String[] args) {
        ArrayQueue<Integer> arrQueue = new ArrayQueue<>(10);
        testQueueIsFilledCorrectly(arrQueue);
        testQueueOverflow(arrQueue);
        testRemoveFromEmptyQueue(arrQueue);
        testQueueUnderflow(arrQueue);
        testQueueSingleton();

        LinkedQueue<Integer> linkedQueue = new LinkedQueue<>(10);
        testQueueIsFilledCorrectly(linkedQueue);
        testQueueOverflow(linkedQueue);
        testRemoveFromEmptyQueue(linkedQueue);
        testQueueUnderflow(linkedQueue);
    }
}
