public abstract class AbstractQueue<E> implements Queue<E> {
    protected final int capacity;
    protected int size = 0;

    public AbstractQueue(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getSize() {
        return size;
    }
}
