public class ArrayQueue<E> extends AbstractQueue<E> {
    private E[] array;
    private int start = 0;
    private int end = 0;

    public ArrayQueue(int capacity) {
        super(capacity);
        array = (E[]) new Object[capacity + 1];
    }

    public boolean add(E element) {
        int oldEnd = end;
        if (incrementEnd()) {
            array[oldEnd] = element;
            size++;
            return true;
        } else {
            return false;
        }
    }

    public E remove() {
        int oldStart = start;
        if (incrementStart()) {
            size--;
            return array[oldStart];
        } else {
            return null;
        }
    }

    private boolean incrementStart() {
        if (start == end) {
            return false;
        } else if (start == array.length) {
            start = 0;
            return true;
        } else {
            start++;
            return true;
        }
    }

    private boolean incrementEnd() {
        if (end == array.length && start == 0) {
            return false;
        } else if (end == array.length) {
            end = 0;
            return true;
        } else {
            end++;
            return true;
        }
    }
}
