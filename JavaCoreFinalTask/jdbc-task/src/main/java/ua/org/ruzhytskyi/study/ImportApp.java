package ua.org.ruzhytskyi.study;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Iterable;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;


public class ImportApp {
    public static String DB_PROP_PATH = "db_properties.conf";

    public static void reCreateTable(Connection con, String tableName)
            throws SQLException {
        String dropSql = "drop table if exists " + tableName;
        String createSql =
            "create table " + tableName + "(" +
            "id serial primary key, " +
            "country_code varchar(2), " +
            "postal_code varchar(20), " +
            "place_name varchar(180), " +
            "admin_name1 varchar(100), " +
            "admin_code1 varchar(20), " +
            "admin_name2 varchar(100), " +
            "admin_code2 varchar(20), " +
            "admin_name3 varchar(100), " +
            "admin_code3 varchar(20), " +
            "latitude varchar(50), " +
            "longitude varchar(50), " +
            "accuracy varchar(1));";

        System.out.println(createSql);

        Statement stmt = con.createStatement();
        stmt.executeUpdate(dropSql);
        stmt.executeUpdate(createSql);
    }

    public static void populateTable(
            Connection con,
            Iterable<CSVRecord> records,
            String tableName) throws SQLException {

        con.setAutoCommit(false);
        String insertSql =
            "insert into " + tableName +
            "(country_code, postal_code, place_name, admin_name1, admin_code1," +
            " admin_name2, admin_code2, admin_name3, admin_code3, latitude," +
            " longitude, accuracy)" +
            " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        PreparedStatement stmt = con.prepareStatement(insertSql);
        for (CSVRecord record: records) {
            stmt.setString(1, record.get(0)); 
            stmt.setString(2, record.get(1)); 
            stmt.setString(3, record.get(2)); 
            stmt.setString(4, record.get(3)); 
            stmt.setString(5, record.get(4)); 
            stmt.setString(6, record.get(5)); 
            stmt.setString(7, record.get(6)); 
            stmt.setString(8, record.get(7)); 
            stmt.setString(9, record.get(8)); 
            stmt.setString(10, record.get(9)); 
            stmt.setString(11, record.get(10)); 
            stmt.setString(12, record.get(11)); 
            stmt.addBatch();
        }
        stmt.executeBatch();
        con.commit();
    }

    public static Properties loadProperties(String propPath)
            throws IOException {
        Properties props = new Properties();
        props.load(new FileReader(propPath));
        return props;
    }

    public static Iterable<CSVRecord> parseCSV(String csvPath)
            throws FileNotFoundException, IOException {
        BufferedReader in = new BufferedReader(new FileReader(csvPath));
        return CSVFormat.TDF.parse(in);
    }

    public static void main(String[] args) {
        Properties props = null;
        Path csvPath = null;

        if (args.length != 1) {
            System.out.println("Wrong number of params. Please, provide only one");
            return;
        } else {
            try {
                csvPath = Paths.get(args[0]);
            } catch (InvalidPathException e) {
                System.out.println("You've provided invalid path");
                return;
            }
        }

        try {
            props = loadProperties(DB_PROP_PATH);
        } catch (IOException e) {
            System.out.println("Failed to load DB properties");
            e.printStackTrace();
            return;
        }

        String conn_url = props.getProperty("conn_url");
        String tableName = csvPath.getFileName().toString();
        tableName = tableName.substring(0, tableName.lastIndexOf("."));

        try (Connection con = DriverManager.getConnection(conn_url, props)) {
            System.out.println("(Re)creating table...");
            reCreateTable(con, tableName);
            System.out.println("Parsing csv...");
            Iterable<CSVRecord> records = parseCSV("./AD.txt");
            System.out.println("Importing records into database...");
            populateTable(con, records, tableName);
            System.out.println("Done");
        } catch (SQLException e) {
            System.out.println("Got exception during database operations");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Failed to parse csv file");
        }
    }
}
