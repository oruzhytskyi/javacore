#!/bin/bash
set -x

DB_USER="study"
DB_PASS="study"
DB_NAME="study00"

echo "Creating dedicated OS user if doesn't exist"
id -u $DB_USER &> /dev/null || useradd $DB_USER

echo "Creating DB user"
exists=$(sudo -u postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$DB_USER'")
if [[ $exists != "1" ]]; then
    sudo -u postgres psql -c "CREATE ROLE $DB_USER PASSWORD '$DB_PASS' INHERIT LOGIN;"
fi

echo "(Re-)Creating DB"
exists=$(sudo -u postgres psql -l | grep $DB_NAME | wc -l)
if [[ $exists == "1" ]]; then
    sudo -u postgres dropdb $DB_NAME
fi
sudo -u postgres createdb --owner=$DB_USER $DB_NAME
