import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;


public class ImplementorApp {
    public static void main(String[] args) {
        if (args.length == 1) {
            implOne(args[0]); 
        } else if (args.length == 0) {
            implMany(getTestClasses());
        } else {
            System.out.println("Wrong number of parameters");
        }
    }

    public static ArrayList<String> getTestClasses() {
        ArrayList<String> classes = new ArrayList<>();
        classes.add("java.util.Set");
        return classes;
    }

    public static void implOne(String testClass) {
        try {
            String[] impl = Implementor.implement(testClass);
            writeToFile(impl, testClass + Implementor.SUFFIX);
        } catch (Exception e) {
            System.out.println(e);
            return;
        }
    }

    public static void implMany(ArrayList<String> testClasses) {
        for (String testClass: testClasses) {
            implOne(testClass);
        }
    }

    public static void writeToFile(String[] impl, String baseImplName) {
        File implDir = new File(baseImplName);
        String className = impl[0];
        String implStr = impl[1];

        if (implDir.exists()) {
            try {
                FileUtils.deleteDirectory(implDir);
            } catch(IOException e) {
                System.out.println("Failed to remove dir: " + implDir);
                e.printStackTrace();
                return;
            }
        }

        try { 
            implDir.mkdir();
        } catch (SecurityException e) {
            System.out.println("Failed to create dir: " + implDir);
            e.printStackTrace();
            return;
        }

        String filePath = implDir + File.separator + className + ".java";
        try (
            FileWriter writer = new FileWriter(filePath);
        ) {
            writer.write(implStr);
        } catch (IOException e) {
            System.out.println("Failed to write file: " + filePath);
            e.printStackTrace();
        }
    }
}
