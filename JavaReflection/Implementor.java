import java.lang.Class;
import java.lang.ClassNotFoundException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.LinkedList;


public final class Implementor {
    public static final String SUFFIX = "Impl";

    private Implementor() {}; 

    public static String[] implement(String className)
            throws ClassNotFoundException {
        String[] retValue = new String[2];
        Class<?> typeToken = Class.forName(className);
        LinkedList<Method> methods = getMethodsToImplement(typeToken);
        LinkedList<Class<?>> types = getMethodsTypes(methods);
        types.add(typeToken);
        String importsStr = generateImports(types);
        String classImplStr = generateClassImpl(typeToken, methods);
        retValue[0] = typeToken.getSimpleName() + SUFFIX;
        retValue[1] = importsStr + "\n" + classImplStr;
        return retValue;
    }

    private static LinkedList<Method>
            getMethodsToImplement(Class<?> typeToken) {
        LinkedList<Method> methods = new LinkedList<>();
        for (Method method: typeToken.getMethods()) {
            int mod = method.getModifiers();
            if (Modifier.isAbstract(mod)) {
                methods.add(method);
            }
        }
        return methods;
    }

    private static Class<?> resolveType(Class<?> type) {
        if (type.isArray()) {
            return type.getComponentType();
        } else {
            return type;
        }
    }

    private static LinkedList<Class<?>>
            getMethodsTypes(LinkedList<Method> methods) {
        LinkedList<Class<?>> types = new LinkedList<>();
        for (Method method: methods) {
            types.add(resolveType(method.getReturnType()));
            for (Class<?> paramType: method.getParameterTypes()) {
                types.add(resolveType(paramType));
            }
        }
        return types;
    }

    private static String generateImports(LinkedList<Class<?>> types) {
        String importsStr = "";
        HashSet<String> typeNames = new HashSet<>();
        for (Class<?> type: types) {
            String canonName = type.getCanonicalName();
            if (!type.isPrimitive() && !typeNames.contains(canonName)) {
                importsStr += "import " + canonName + ";\n";
                typeNames.add(canonName);
            }
        } 
        return importsStr;
    }

    private static String generateClassImpl(   
            Class<?> typeToken, LinkedList<Method> methods) {
        String className = typeToken.getSimpleName();
        String implStr = "public class " +
                         className + SUFFIX + " implements " +
                         className + " {\n";
        for (Method method: methods) {
            implStr += generateMethodImpl(method);
        }
        implStr += "}";
        return implStr;
    }

    private static String generateMethodImpl(Method method) {
        Class<?> retType = method.getReturnType();
        String retTypeStr = retType.getSimpleName(); 
        String methodName = method.getName();
        int modifiers = method.getModifiers() & ~Modifier.ABSTRACT;
        String modifStr = Modifier.toString(modifiers);
        String methodStr = "    " + modifStr + " " +
                           retTypeStr + " " + methodName + "(";

        for (int i = 0; i < method.getParameterCount(); i++) {
            Class<?> param = method.getParameterTypes()[i];
            methodStr += param.getSimpleName() + " param" + i;
            if (i != method.getParameterCount() - 1) {
                methodStr += ", ";
            }
        }
        methodStr += ")";

        Class<?>[] exceptionTypes = method.getExceptionTypes();
        if (exceptionTypes.length != 0) {
            methodStr += " throws ";
            for (int i = 0; i < exceptionTypes.length; i ++) {
                Class<?> exType = exceptionTypes[i];
                methodStr += exType.getSimpleName();
                if (i != exceptionTypes.length - 1) {
                    methodStr += ", ";
                }
            }
        }
        methodStr += " {\n";

        if (retType.equals(void.class)) {
            // This check is necessary, because isPrimitive returns True
            // for void. Just don't return anything in this case.
        } else if (retType.isPrimitive()) {
            Object retValue = PrimitiveDefaults.getDefaultValue(retType);
            methodStr += "        return " + retValue + ";\n";
        } else {
            methodStr += "        return null;\n";
        }
        methodStr += "    }\n";

        return methodStr;
    }
}
