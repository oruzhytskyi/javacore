import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;


public class Calc {
    private static final String inputFile = "input.txt";
    private static final String outputFile = "output.txt";

    public static void main(String[] args) {
        try {
            Function func = new Function(new File(inputFile));
            func.write(new File(outputFile));
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e);
        } catch (IOException e) {
            System.out.println("Something went wrong: " + e);
        } catch (SyntaxErrorException e) {
            System.out.println("Syntax error: " + e);
        }
    }
}
