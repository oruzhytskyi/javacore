import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.util.ArrayList;

import JavaAcademy.JavaCore.ErrorHandlingWithExceptions.Function;
import JavaAcademy.JavaCore.ErrorHandlingWithExceptions.SyntaxErrorException;
import JavaAcademy.JavaCore.ErrorHandlingWithExceptions.DBZException;
import JavaAcademy.JavaCore.ErrorHandlingWithExceptions.OverflowException;


public class FileCalc {
    private static final int DELIM_LENGTH = 20;

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please, provide input and output files");
            return;
        } else if (args.length < 2) {
            System.out.println("Please, provide output file as second param");
        }

        File inputFile = new File(args[0]);
        File outputFile = new File(args[1]);

        try (BufferedReader input = new BufferedReader(
                                new FileReader(inputFile));
             PrintWriter output = new PrintWriter(
                                new FileWriter(outputFile, false));) {

            String line = null;
            int lineNumber = 0;
            ArrayList<Function> functions = new ArrayList<>();

            while ((line = input.readLine()) != null) {
                lineNumber++;
                try {
                    functions.add(new Function(line));
                } catch (SyntaxErrorException e) {
                    System.out.println("Syntax error at line " + lineNumber);
                }
            }

            for (Function func: functions) {
                output.print(makeDelim(func.toString().length()) + func);
            }
            output.println();

            for (int x = 0; x < 11; x++) {
                output.print(x);
                for(Function func: functions) {
                    try {
                        int val = func.evaluate(x);
                        int valLength = String.valueOf(val).length();
                        output.print(makeDelim(valLength) + val);
                    } catch (DBZException e) {
                        String msg = "division by zero";
                        output.print(makeDelim(msg.length()) + msg);
                    } catch (OverflowException e) {
                        String msg = "overflow";
                        output.print(makeDelim(msg.length()) + msg);
                    }
                }
                output.println();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + e);
        } catch (IOException e) {
            System.out.println("Something went wrong: " + e);
        }
    }

    private static String makeDelim(int valueLen) {
        int maxLen = Math.max(DELIM_LENGTH, valueLen + 1);
        String spaces = "";
        for (int i = 0; i < maxLen - valueLen; i++) spaces += " ";
        return spaces;
    }
}
