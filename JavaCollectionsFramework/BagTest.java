import java.util.Collection;


public class BagTest {
    public static void main(String[] args) {
        Integer[] data1 = {1, 5, 7, 8, 1, 12, 1};
        Integer[] data2 = {1, 7, 5, 8, 1, 12, 1};
        testBagFilled(makeLinkedBag(data1));
        testElementRemoved(makeLinkedBag(data1));
        testBagsEqual(makeLinkedBag(data1), makeLinkedBag(data2));

        testBagFilled(makeBag(data1));
        testElementRemoved(makeBag(data1));
        testBagsEqual(makeBag(data1), makeBag(data2));
    }

    public static void testBagFilled(Collection<?> bag) {
        dumpBag(bag);
    }

    public static void testElementRemoved(Collection<?> bag) {
        dumpBag(bag);
        bag.remove(1);
        dumpBag(bag);
        bag.remove(1);
        bag.remove(1);
        dumpBag(bag);
    }

    public static void testBagsEqual(Collection<?> bag1, Collection<?> bag2) {
        dumpBag(bag1);
        dumpBag(bag2);
        System.out.println(bag1.equals(bag2));
    }

    public static <T> void dumpBag(Collection<T> bag) {
        boolean first = true;
        for (T i : bag) {
            if (first) {
                first = false; 
            } else {
                System.out.print(",");
            }
            System.out.print(i);
        }
        System.out.println();
    }

    public static <T> LinkedBag<T> makeLinkedBag(T[] elems) {
        LinkedBag<T> bag = new LinkedBag<>();
        for (T e: elems) {
            bag.add(e); 
        }
        return bag;
    }

    public static <T> Bag<T> makeBag(T[] elems) {
        Bag<T> bag = new Bag<>();
        for (T e: elems) {
            bag.add(e); 
        }
        return bag;
    }
}
