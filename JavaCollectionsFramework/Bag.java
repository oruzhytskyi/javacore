import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;


public class Bag<E> extends AbstractSet<E> implements Collection<E> {
    private LinkedList<E> bag = new LinkedList<>();

    public boolean add(E e) {
        int index = bag.indexOf(e);
        if (index != -1) {
            bag.add(index, e);
        } else {
            bag.add(e);
        }
        return true;
    }

    public Iterator<E> iterator() {
        return bag.iterator();
    }

    public int size() {
        return bag.size();
    }
}
