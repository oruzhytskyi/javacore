import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;


public class LinkedBag<E> extends AbstractSet<E> implements Collection<E> {
    private LinkedList<E> linkedBag = new LinkedList<>();

    public boolean add(E e) {
        return linkedBag.add(e);
    }

    public Iterator<E> iterator() {
        return linkedBag.iterator();
    }

    public int size() {
        return linkedBag.size();
    }
}
